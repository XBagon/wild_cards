mod cards;
mod events;
mod zones;

use crate::game::cards::{CardBase, Instance};
use crate::game::events::{Attack, AttackSystem};
use cards::{heroes, minions};
use specs::prelude::*;

pub fn initialize() -> World {
    let mut world = World::new();
    world.register::<CardBase>();
    world.register::<Instance>();
    events::initialize(&mut world);
    minions::initialize(&mut world);
    heroes::initialize(&mut world);
    /*
    let mut attack_system = AttackSystem {
        attacker: (),
        attacked: ()
    };
    attack_system.run_now(&mut world.res);
    */
    world
}
