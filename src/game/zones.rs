use specs::prelude::*;
use specs_derive::Component;

#[derive(Clone, Component, Debug, Default)]
#[storage(NullStorage)]
pub struct Deck;

#[derive(Clone, Component, Debug, Default)]
#[storage(NullStorage)]
pub struct Grave;

#[derive(Clone, Component, Debug, Default)]
#[storage(NullStorage)]
pub struct Hand;
