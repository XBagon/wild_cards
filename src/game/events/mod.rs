use specs::prelude::*;
use specs_derive::Component;
use std::fmt;
use std::marker::PhantomData;

pub fn initialize(mut world: &mut World) {
    world.register::<Attack<Before>>();
    world.register::<Attack<On>>();
    world.register::<Attack<After>>();
}

#[derive(Clone, Component)]
#[storage(DenseVecStorage)]
pub struct Attack<T: 'static + Timing + Sync + Send> {
    pub marker: PhantomData<T>,
    pub handler: fn(&mut World, Entity, Entity),
}

impl<T: 'static + Timing + Sync + Send> fmt::Debug for Attack<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Attack")
    }
}

pub struct AttackSystem {
    attacker: Entity,
    attacked: Entity,
}

impl<'a> System<'a> for AttackSystem {
    type SystemData = (Entities<'a>, ReadStorage<'a, Attack<Before>>, ReadStorage<'a, Attack<On>>, ReadStorage<'a, Attack<After>>);

    fn run(&mut self, (entities, before, on, after): Self::SystemData) {
        for (ent, event) in (&entities, &before).join() {}

        for (ent, event) in (&entities, &on).join() {}

        for (ent, event) in (&entities, &after).join() {}
    }
}

/*
#[derive(Clone, Component, Debug)]
#[storage(DenseVecStorage)]
pub struct Attacked<T: Timing, U: Target> {
    pub handler: Fn(&mut World, Entity, Entity),
}


#[derive(Clone, Component, Debug)]
#[storage(DenseVecStorage)]
pub struct Damage<T: Timing, U: Target>;
*/

pub trait Timing {}

pub struct Before;
pub struct On;
pub struct After;

impl Timing for Before {}
impl Timing for On {}
impl Timing for After {}
