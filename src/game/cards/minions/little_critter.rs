use super::{super::*, *};
use crate::game::events::{Attack, On};
use specs::prelude::*;
use std::marker::PhantomData;

pub fn initialize(world: &mut World) -> Entity {
    world
        .create_entity()
        .with(CardBase {
            name: String::from("Little Critter"),
            cost: 2,
        })
        .with(MinionBase {
            max_health: 2,
            health: 2,
            attack: 3,
        })
        //
        .with(Attack::<On> {
            marker: PhantomData,
            handler: attack,
        })
        .build()
}

fn attack(world: &mut World, attacker: Entity, attacked: Entity) {
    eprintln!("attack!");
}
