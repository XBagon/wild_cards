use super::{super::*, *};
use crate::game::events::{Attack, On};
use specs::prelude::*;
use std::marker::PhantomData;

pub fn initialize(world: &mut World) -> Entity {
    world
        .create_entity()
        .with(CardBase {
            name: String::from("Big Critter"),
            cost: 2,
        })
        .with(MinionBase {
            max_health: 7,
            health: 7,
            attack: 8,
        })
        //
        .with(Attack::<On> {
            marker: PhantomData,
            handler: attack,
        })
        .build()
}

fn attack(world: &mut World, attacker: Entity, attacked: Entity) {
    eprintln!("attack!");
}
