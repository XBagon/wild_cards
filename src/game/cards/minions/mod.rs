pub mod big_critter;
pub mod little_critter;

use crate::game::cards::CardBase;
use crate::game::events::Attack;
use specs::prelude::*;
use specs_derive::Component;

#[derive(Clone, Component, Debug)]
#[storage(DenseVecStorage)]
pub struct MinionBase {
    pub max_health: i32,
    pub health: i32,
    pub attack: i32,
}

pub fn initialize(mut world: &mut World) {
    world.register::<MinionBase>();
    let test0 = little_critter::initialize(&mut world);
}
