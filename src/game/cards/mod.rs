pub mod heroes;
pub mod minions;

use specs::prelude::*;
use specs_derive::Component;

#[derive(Clone, Component, Debug)]
#[storage(DenseVecStorage)]
pub struct CardBase {
    pub name: String,
    pub cost: i32,
}

#[derive(Clone, Component, Debug)]
#[storage(DenseVecStorage)]
pub struct Instance {
    pub template: Entity,
}
