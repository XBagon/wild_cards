pub mod neutralist;

use specs::prelude::*;
use specs_derive::Component;

#[derive(Clone, Component, Debug)]
#[storage(DenseVecStorage)]
pub struct HeroBase {
    pub max_mana: i32,
    pub mana: i32,
    pub max_health: i32,
    pub health: i32,
    pub attack: i32,
}

pub fn initialize(mut world: &mut World) {
    world.register::<HeroBase>();
    neutralist::initialize(&mut world);
}
