use super::{super::*, *};
use specs::prelude::*;

pub fn initialize(world: &mut World) -> Entity {
    world
        .create_entity()
        .with(CardBase {
            name: String::from("Neutralist"),
            cost: 2,
        })
        .with(HeroBase {
            max_mana: 0,
            mana: 0,
            max_health: 2,
            health: 2,
            attack: 3,
        })
        .build()
}
